# LTI Tool Provider (Example)

An example LTI provider application to illustrate how LTI works.
This application is intended to work with the [LTI Tool Consumer](https://gitlab.com/sonofborge/lti-tool-provider)
project in order to illustrate how LTI launches work.

## Getting Started

1. `rbenv install`
1. `gem install bundler`
1. `bundle install`
1. `shotgun -p 9494`
