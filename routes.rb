#
# GET Home
#
get '/' do
  erb :index
end

#
# POST LTI Tool Provider
#
# This is for launching the tool.
# It will verify the OAuth signature.
#
post '/lti_tool' do
  return erb :error unless authorize!

  # If this is an outcome service (assignment)
  if @tool_provider.outcome_service?
    # It's a launch for grading
    erb :assignment
  else
    # Normal Tool Launch
    signature = OAuth::Signature.build(request, :consumer_secret => @tool_provider.consumer_secret)

    @signature_base_string = signature.signature_base_string
    @secret = signature.send(:secret)

    @tool_provider.lti_msg = "This concludes the basic LTI launch example."
    erb :basic_tool
  end
end

#
# POST Assignment Results
#
post '/assignment' do
  launch_params = request['launch_params']

  if launch_params
    key = launch_params['oauth_consumer_key']
  else
    show_error "The tool never launched"
    return erb :error
  end

  @tool_provider = IMS::LTI::ToolProvider.new(key, $oauth_creds[key], launch_params)

  unless @tool_provider.outcome_service?
    show_error "This tool wasn't launched as an outcome service"
    return erb :error
  end

  # Post the given score to the Tool Consumer
  @score = (params['score'] != '' ? params['score'] : nil)
  response = @tool_provider.post_replace_result!(@score)

  if response.success?
    @tool_provider.lti_msg = "Your score is #{@score}."
    erb :assignment_finished
  else
    @tool_provider.lti_errormsg = "The Tool Consumer failed to add the score."
    show_error "Your score was not recorded: #{response.description}"
    return erb :error
  end
end
